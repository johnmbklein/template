import org.junit.*;
import static org.junit.Assert.*;
import java.io.*;
import java.util.ArrayList;


public class ClassTest {

  @Test
  public void checka_AllergiesScorer_1(){
    Allergies testAllergies = new Allergies();
    ArrayList<String> testAllergyArray = new ArrayList();
    testAllergyArray.add("eggs");
    Integer expected = 1;
    assertEquals(expected, testAllergies.allergiesScore(testAllergyArray));
  }

}
